<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    use HasFactory;

      // los campos de asignacion masiva
    // son los campos que se almacenan en el modelo sin
    // tener que ir de 1 en 1
    protected $fillable = [
        'nombre',
        'apellidos',
        'direccion',
        'telefono',
        'email'
    ];

}
