<?php

namespace App\Http\Controllers;

use App\Models\Clientes;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    public function index()
    {
        //tengo que leer todos los clientes y mandarlos a la vista
        //$clientes = Cliente::all(); // array de clientes
        $clientes = Clientes::paginate(6);

        return view('clientes.index', [
            "clientes" => $clientes // le paso a la vista el array de clientes
        ]);
    }


    public function show($id)
    {
        $cliente = Clientes::find($id);

        return view('clientes.show', [
            "cliente" => $cliente
        ]);
    }


    /**
     * cargar el formulario de editar
     */
    public function edit($id)
    {

        // recupero los datos del cliente a editar
        $cliente = Clientes::find($id);

        //mostrar el formulario para editar el cliente
        return view('clientes.edit', [
            'cliente' => $cliente
        ]);
    }

     //cuando pulso el boton de editar
    //recibe el id pero solo el cliente tiene el id
    public function update(Request $request, Clientes $cliente)
    {
        //validamos los datos
        $request->validate([
            'nombre' => 'required',
            'apellidos' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'email' => 'required',
        ]);
        //actualizamos al cliente
        $cliente->update($request->all());

        //redireccionamos a la vista show
        return redirect()
            ->route('clientes.show', $cliente->id)
            ->with('mensaje', "El cliente se ha actualizado correctamente");
    }

    public function create()
    {
        //nos carga un formulario para crear un nuevo cliente
        return view('clientes.create');
    }

    // cuando en el formulario de crear registro
    // pulso el boton de insertar
    // me llega a este metodo
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'apellidos' => 'required',
            'direccion' => 'required',
            'telefono' => 'required',
            'email' => 'required',
        ]);

        // asignacion masiva
        $cliente = Clientes::create($request->all());
        // sin asignacion masiva
        // $cliente= new Cliente();
        // $cliente->nombre = $request->input('nombre');
        // $cliente->email = $request->input('email');
        // $cliente->save();

        // redireccionar a la vista show
        return redirect()
            ->route('clientes.show', $cliente->id)
            ->with('mensaje', 'El cliente se ha creado correctamente');
    }

    /**
     * borrar el cliente
     */
    public function destroy(Clientes $cliente)
    {
        //eliminar el cliente
        $cliente->delete();
        //redireccionar a la vista index
        return redirect()
            ->route('clientes.index')
            ->with('mensaje', 'El cliente se ha eliminado correctamente');
    }
}
