@extends('layouts.main')

@section('titulo', 'Mostrar')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Ver cliente</h1>
            <p class="col-lg-10 fs-4">
                Datos solicitados del cliente
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    @if (session('mensaje'))
        <div class="row m-2">
            <div class="alert alert-info">
                {{ session('mensaje') }}
            </div>
        </div>
    @endif
    <div class="row mt-3">
        <div class="col">
            <div class="card shadow-xl text-bg-warning">
                <div class="card-body">
                    <h5 class="card-title">
                        {{ $cliente->id }}
                    </h5>
                    <p class="card-text">
                        Nombre: {{ $cliente->nombre }}
                    </p>
                    <p class="card-text">
                        Apellidos: {{ $cliente->apellidos }}
                    </p>
                    <p class="card-text">
                        Direccion: {{ $cliente->direccion }}
                    </p>
                    <p class="card-text">
                        Telefono: {{ $cliente->telefono }}
                    </p>
                    <p class="card-text">
                        Email: {{ $cliente->email }}
                    </p>
                    <div class="card-footer">
                        <div class="vertical-align-bottom d-flex justify-content-between align-items-center">
                            <a href="{{ route('clientes.edit', $cliente->id) }}" class="btn btn-primary">Editar</a>
                            <form action="{{ route('clientes.destroy', $cliente) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Eliminar</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

