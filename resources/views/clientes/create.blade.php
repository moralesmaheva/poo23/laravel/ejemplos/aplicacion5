@extends('layouts.main')

@section('titulo', 'Nuevo')

@section('cabecera')
    <section class="pt-5 container">
        <div class="row py-lg-5">
            <h1 class="display-4 fw-bold lh-1 mb-3">Nuevo cliente</h1>
            <p class="col-lg-10 fs-4">
                Desde este formulario puedes crear un nuevo cliente
            </p>
        </div>
    </section>
    @parent
@endsection

@section('contenido')
    {{-- validamos los errores --}}
    @if ($errors->any())
        <div class="row mt-3">
            <h2>Error en el formulario</h2>
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    @endif
    <div class="row mt-3">
        <div lass="col-lg-10 mt-3 mx-auto">
            <form action="{{ route('clientes.store') }}" method="post" class="p-4 p-md-5 border rounded-3 bg-light">
                @csrf
                {{-- para que no borre los datos correctos cuando hay un error usamos el old en el value --}}
                <div class="mb-3">
                    <label for="nombre" class="form-label">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre') }}">
                </div>
                <div class="mb-3">
                    <label for="apellidos" class="form-label">Apellidos</label>
                    <input type="text" class="form-control" id="apellidos" name="apellidos" value="{{ old('apellidos') }}">
                </div>
                <div class="mb-3">
                    <label for="direccion" class="form-label">Dirección</label>
                    <input type="text" class="form-control" id="direccion" name="direccion" value="{{ old('direccion') }}">
                </div>
                <div class="mb-3">
                    <label for="telefono" class="form-label">Teléfono</label>
                    <input type="text" class="form-control" id="telefono" name="telefono" value="{{ old('telefono') }}">
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                </div>

                <button type="submit" class="btn btn-primary">Insertar</button>

        </div>

    @endsection
