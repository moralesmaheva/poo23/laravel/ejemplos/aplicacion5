<?php


use App\Http\Controllers\ClientesController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

// pagina de inicio
Route::controller(HomeController::class)->group(function () {
    Route::get('/', 'index')->name('home.index');
    Route::get('/index', 'index')->name('home.index');
});

// acciones del controlador de clientes

Route::controller(ClientesController::class)->group(function () {
    Route::get('/clientes', 'index')->name('clientes.index');
    Route::get('/clientes/create', 'create')->name('clientes.create');
    Route::get('/clientes/{id}', 'show')->name('clientes.show');
    Route::post('/clientes', 'store')->name('clientes.store');
    Route::get('/clientes/edit/{id}', 'edit')->name('clientes.edit');
    Route::put('/clientes/{cliente}', 'update')->name('clientes.update');
    Route::delete('/clientes/{cliente}', 'destroy')->name('clientes.destroy');
});



